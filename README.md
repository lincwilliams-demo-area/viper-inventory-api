**Operation Golden Brown Bear**

This demo deploys a java application that provides an API to the Colonial Defense Forces Current Viper Inventory.  It is designed as a Honey Pot for Cylon cyber attacks.

to use this Template you will need:

 - An active AWS account
 - Beanstock service
 - S3 service
 - postman or other API test software/process
 
***NOTE: Beware AWS costs** 
 - *AWS is only free for low volumes* 
 - *I recommend you set up a budget inside the AWS console to help you manage the costs*
 - ***Beanstock service can get expensive when if you do not monitor it****



| Variable              | Value    | Notes      |   
|-----------------------|----------|------------|
| AWS_ACCESS_KEY_ID     | from aws IAM | See Below |
| AWS_SECRET_ACCESS_KEY |     from aws  IAM   | See Below           |
| S3_BUCKET             |     from aws     | S3 bucket name           |
| ARTIFACT_NAME          |      Your project    |      unquie name of the project jar      |
| APP_NAME             |     AWS Beanstock     |     Beanstock>       |
| ENV_NAME             |     AWS Beanstock    |            |
| POSTMAN_COLLECTION_FILE             |     Postman     |     file name of your postman test in the repo - used in the API test Job     |
| POSTMAN_ENV_FILE             |   Postman       |    file name of your postman environment config export in the repo        |


Getting the AWS Access info:
1.  Open the  [AWS Console](https://console.aws.amazon.com/)
2. Select **IAM SERVICE**
3.  Click on  **Users**  in the sidebar
4.  Click on your username or the account you are using
5.  Click on the  **Security Credentials**  tab
6.  Click  **Create Access Key**
7.  Click  **Show User Security Credentials**
8. Record the **access key id and secret value**

**POSTMAN**
 1. Load the config file in the repo in Postman
 2. Update your postman environments to reflect a connection to your new Beanstock environment - base url for the environment 
 3. Export your environment file (make sure the file name has no spaces)
 4. Import into your repo
 5. Update your variables: POSTMAN_ENV_FILE  & POSTMAN_COLLECTION_FILE
 

Note: If you are deploying the same template to mutiple sites using the same aws settings you will need to clean up the Application versions in AWS Beanstock or you will get deployment errors about versions already exist
